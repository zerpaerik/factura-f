<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quantum</title>
        
    <script src="{{ URL::asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ URL::asset('js/sweetalert.min.js') }}"></script>
    <link rel="shortcut icon" href="logo.ico"/>
    <!-- <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" /> -->
    <link rel="stylesheet" href="{!! asset('css/sweetalert.css') !!}" />
    <!-- <link rel="stylesheet" href="{!! asset('css/app.css') !!}" /> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style>
        /*imagen de portada*/
        body.Portada {
          background-image: url("/img/facturak-portada.png");
          background-size: cover;
        }
    </style>
</head>
<body class="Portada">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <!-- Main view  -->
        @yield('content')

        </div>
    <!-- End middle-->
    <script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

@section('scripts')
@show

</body>
</html>
