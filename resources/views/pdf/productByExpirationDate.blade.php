	 <head>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">    
    
    <style type="text/css">
      div.border{
        border: 1px solid black;
      }
    </style>
    
    <meta charset="utf-8"> 

  </head>

    <table style="width: 100%">
      <tr>
        <td style="width: 47%;">
          <table>
            <tbody>        
              
              <tr>        
                <td>
                    <div name="logo" style="width: 300px; height: 100px">                    
                        <img src="data:image/png;base64,{{$resultObject->company->logo}}" style="margin-top: -15px"> 
                    </div>
                </td>
              </tr>

              <tr>
                <td height="20px"></td>
              </tr>
            
            </tbody>  
          </table>
        </td>

        <td style="width: 6%">          
        </td>
        
        <td style="width: 47%; vertical-align:top;">
          <table border="1">
            <tbody>                    
              <tr>        
                <td>
                  <table border="0" style="margin-left: 5px; margin-right: 5px">
                    <tr>
                      <td><div style="font-weight: bold; font-size: 11px">R.U.C:</div></td>
                    </tr>
                    <tr>
                      <td style="font-size: 11px">{{$resultObject->company->ruc}} </td>
                    </tr>

                    <tr>
                      <td height="5px"></td>
                    </tr>
                    
                    <tr>
                      <td><div style="font-weight: bold; font-size: 11px">NOMBRE COMPAÑIA:</div></td>
                    </tr>
                    <tr>
                      <td style="font-size: 11px">{{$resultObject->company->name}}</td>                      
                    </tr>
                    
                    <tr>
                      <td height="5px"></td>
                    </tr>
                    
                    <tr>
                      <td><div style="font-weight: bold; font-size: 11px">NOMBRE COMERCIAL:</div></td>
                    </tr>
                    <tr>
                      <td style="font-size: 11px">{{$resultObject->company->comercial_name}}</td>
                    </tr>

                    <tr>
                      <td height="5px"></td>
                    </tr>

                    <tr>
                      <td style="font-size: 11px"><div style="font-weight: bold; font-size: 11px">Dir. Matriz: </div>{{$resultObject->company->address}}</td>
                    </tr>

                    <tr>
                      <td height="5px"></td>
                    </tr>

                    <tr>
                      <td style="font-size: 11px"><div style="font-weight: bold; font-size: 11px">Sucursal: </div>{{$resultObject->branch->name}}</td>
                    </tr>
                    
                    <tr>
                      <td height="5px"></td>
                    </tr>
                    <tr>
                      <td style="font-size: 11px"><div style="font-weight: bold; font-size: 11px">Dir. Sucursal: </div>{{$resultObject->branch->address}}</td>
                    </tr>
                    
                    <tr>
                      <td height="5px"></td>
                    </tr>
                  </table>
                </td>
              </tr>                          
            </tbody>  
          </table>          
        </td>
      </tr>
    </table>

    <br>                 

    <table width="100%" style="border:1px solid black">
      <tbody>
        <tr>
          <td style="font-size: 11px; text-align: center; font-weight: bold; padding: 10px 0;">LISTADO DE PRODUCTOS CON FECHA DE VENCIMIENTO ENTRE {{Carbon\Carbon::parse($resultObject->range->startDate)->format('d-m-Y')}} - {{Carbon\Carbon::parse($resultObject->range->endDate)->format('d-m-Y')}}</td>
        </tr>
      </tbody>
    </table>      
    
    <br><br>

    <table width="100%" border="0">
      <thead>
        <tr>
          <th style="width: 7%; font-size: 11px"><center>Código</center></th>
          <th style="width: 22%; font-size: 11px"><center>Producto</center></th>
          <th style="width: 17%; font-size: 11px"><center>Generico</center></th>
          <th style="width: 8%; font-size: 11px"><center>Cantidad</center></th> 
          <th style="width: 12%; font-size: 11px"><center>Fecha Vencimiento</center></th>
          <th style="width: 17%; font-size: 11px"><center>Laboratorio</center></th>
          <th style="width: 17%; font-size: 11px"><center>Proveedor</center></th>
        </tr>
      </thead>  
      <tbody>
        @foreach($resultObject->products as $product)
          <tr style="">
            <td style="font-size: 10px; padding: 5px 0;"><center>{{$product->principal_code}}</center></td>
            <td style="font-size: 10px; padding: 5px 0;"><center>{{$product->name}}</center></td>
            <td style="font-size: 10px; padding: 5px 0;"><center>{{$product->generic}}</center></td>
            <td style="font-size: 10px; padding: 5px 0;"><center>{{$product->amount_send}}</center></td> 
            <td style="font-size: 10px; padding: 5px 0;"><center>{{$product->expiration_date}}</center></td>
            <td style="font-size: 10px; padding: 5px 0;"><center>{{$product->laboratory}}</center></td>
            <td style="font-size: 10px; padding: 5px 0;"><center>{{$product->supplier}}</center></td>
          </tr>
        @endforeach
      </tbody>
    </table> 
