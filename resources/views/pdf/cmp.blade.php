	<head>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">		
		
		<style type="text/css">
			.table-main{
			 margin:0 -30px;
			}
  	</style>
    
    <meta charset="utf-8"> 

	</head>

    <table class="table-main">
      <tr>
        

        <td style="width: 100%">
          <table>
            <tbody>        
              
              <tr>        
                <td>
                  
                  <table style="width: 100%">
                    <tr>
                     <td style="font-size: 11px;"><div style="font-weight: bold; font-size: 11px">R.U.C.:</div> {{$invoiceObject->company->ruc}}</td>
                    </tr>

                    <tr>
                      <td height="5px"></td>
                    </tr>

                    <tr>
                      <td><div style="font-weight: bold; font-size: 11px">F A C T U R A</div></td>
                    </tr>
					@if($invoiceObject->branch->id == 16 && $invoiceObject->invoice->type == 2)
                    <tr>
                      <td style="font-size: 11px">No. {{"004" . "-" . "002" . "-" . $invoiceObject->invoice->principal_code}}</td>
                    </tr>
					@elseif($invoiceObject->branch->id == 16 && $invoiceObject->invoice->type == 1)
					<tr>
                      <td style="font-size: 11px">No. {{"003" . "-" . "002" . "-" . $invoiceObject->invoice->principal_code}}</td>
                    </tr>
                    @elseif($invoiceObject->branch->id == 17 && $invoiceObject->invoice->type == 1)
                    <tr>
                      <td style="font-size: 11px">No. {{"001" . "-" . "002" . "-" . $invoiceObject->invoice->principal_code}}</td>
                    </tr>
					@elseif($invoiceObject->branch->id == 17 && $invoiceObject->invoice->type == 2)
                    <tr>
                      <td style="font-size: 11px">No. {{"002" . "-" . "002" . "-" . $invoiceObject->invoice->principal_code}}</td>
                    </tr>
					@else
                    @endif					

                    <tr>
                      <td height="5px"></td>
                    </tr>

                    <tr>
                      <td><div style="font-weight: bold; font-size: 11px">Número de Autorización</div></td>
                    </tr>
                    <tr>
                      <td style="font-size: 11px">{{$invoiceObject->invoice->auth_code}}</td>
                    </tr>

                    <tr>
                      <td height="5px"></td>
                    </tr>

                    <tr>
                      <td style="font-size: 11px">{{$invoiceObject->invoice->auth_date ? $invoiceObject->invoice->auth_date : ''}}</td>
                    </tr>

                    <tr>
                      <td height="5px"></td>
                    </tr>

                  </table>
                  
                </td>
              </tr>                          
            </tbody>  
          </table>          
        </td>
      </tr>
    </table>
    <br>                 
    <table width="100%" style="border:1px solid black" class="table-main">
      <tbody>
        <tr>
          <td style="width: 1%"></td>
          @if($invoiceObject->client->social_reason != NULL)
            <td style="width: 70%; font-size: 11px">Razón social / Nombres y Apellidos: <br>{{$invoiceObject->client->social_reason}}</td>
          @else
            <td style="width: 70%; font-size: 11px">Razón social / Nombres y Apellidos: <br>{{$invoiceObject->client->comercial_name}}</td>
          @endif
          <td style="width: 1%"></td>
          <td style="width: 25%; font-size: 11px">            
              {{$invoiceObject->client->idClass}}:
              {{$invoiceObject->client->identification}}                          
          </td>          
        </tr>
        <tr>
          <td style="width: 1%"></td>
          <td style="font-size: 11px">Dirección: {{$invoiceObject->client->address}}</td>
          <td></td>
          <td style="font-size: 11px">Teléfono: {{$invoiceObject->client->phone}}</td>
        </tr>
        <tr>
          <td style="width: 1%"></td>
          <td style="font-size: 11px">Fecha de Emisión: {{$invoiceObject->invoice->invoice_date}}</td>
          <td></td>
          <td></td>
        </tr>
      </tbody>
    </table>      
    
    <br><br>

    <table width="100%" class="table-main">
      <thead>
        <tr>
          <th style="width: 20%; font-size: 10px"><center>Cantidad.</center></th>
          <th style="width: 35%; font-size: 10px"><center>Descripcion.</center></th>            
          <th style="width: 15%; font-size: 10px"><center>PrecioUnit.</center></th>            
          <th style="width: 15%; font-size: 10px"><center>PrecioTotal</center></th>
        </tr>
      </thead>  
      <tbody>
        @foreach($invoiceObject->invoice_line as $line)
          <tr>
            <td style="font-size: 5px;width: 20%; margin-right:10px;" align="center">{{(int)$line->quantity}}</td>
            <td style="font-size: 5px;width: 35%; margin-right:10px;" align="center">{{$line->name}}</td>
            <td style="font-size: 5px;width: 15%; margin-right:10px;" align="center">{{number_format($line->unit_price, 2, ',', '.')}}</td>
            <td style="font-size: 5px;width: 15%" align="center">{{$line->totalCostNoTax}}</td>
          </tr>
        @endforeach
      </tbody>
    </table> 

    <br>
    <table width="100%" class="table-main">
      <tbody>
        <tr>
         
          <td style="width: 100%">
            <table width="100%">      
              <tbody>                
                <tr>                  
                  
                  <td>
                    <tr> 
                      <td style="font-weight: bold;  font-size: 5px" align="center">SUBTOTAL 12%</td> 
                      <td align="right" style="font-size: 5px">${{$invoiceObject->invoice->total_invoice}}</td>
                    </tr> 
                  </td>

                  <td>
                    <tr> 
                      <td style="font-weight: bold;  font-size: 5px" align="center">DESCUENTO</td> 
                      <td align="right" style="font-size: 5px">${{$invoiceObject->invoice->total_discount}}</td>
                    </tr> 
                  </td>                  

                  <td>
                    <tr> 
                      <td style="font-weight: bold;font-size: 5px" align="center">IVA 12%</td> 
                      <td align="right" style="font-size: 5px">${{$invoiceObject->invoice->total_12}}</td>
                    </tr> 
                  </td>

                  <td>
                    <tr> 
                      <td style="font-weight: bold;font-size: 5px" align="center">VALOR TOTAL</td> 
                      <td align="right" style="font-size: 5px">${{$invoiceObject->invoice->total_invoice}}</td>
                    </tr> 
                  </td>
                </tr>

              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  