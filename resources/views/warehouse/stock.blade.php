@extends('layouts.app')

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Inventario</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('home') }}">Inicio</a>
                </li>
                <li>
                    <a href="{{ url('/warehouse') }}">Inventario</a>
                </li>
                <li class="active">
                    <strong>Stock de Productos</strong>
                </li>
            </ol>
        </div>        
    </div>

    <div id="stock" class="wrapper wrapper-content animated fadeInRight" ng-controller="WarehouseController as vm">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Stock de Productos</h5>
                        <a href="/warehouse/stockPDF" class="btn btn-info pull-right" style="margin-top: -8px"><i class="fa fa-download" aria-hidden="true"></i> Descargar Stock</a>
                    </div>                    

                    <div class="ibox-content" >
                        <div class="table-responsive">
		                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap" style="width: 98%">
			                    <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="myTable" aria-describedby="DataTables_Table_0_info" role="grid" >
			                        <thead>
			                        
			                            <tr role="row">
			                            
			                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" style="width: 10%;"><center>Código</center></th>

			                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" style="width: 10%;"><center>Cantidad</center></th>

			                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 35%;"><center>Nombre Comercial</center></th>

			                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 30%;"><center>Generico</center></th>

			                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 5%;"><center>PVP</center></th>

			                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 5%;"><center>Estado</center></th>

			                                <!-- <th class="sorting" rowspan="1" colspan="1" style="width: 15%;"><center>Acciones</center>   </th> -->
			                            </tr>
			                        
			                        </thead>

			                        <tbody>
			                        </tbody>    
			                    </table>
			                </div>
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    </div> 

    <script>
            
            var apiUrl =  '{{env('API_HOST', NULL)}}/warehouseStockDt/{{$_COOKIE['company_id']}}/{{$_COOKIE['branch_office_id']}}';         
            
            $(document).ready( function () {
                $('#myTable').DataTable({
                    "processing" : false,
                    "serverSide" : true,
                    "language": {
                                  "url": "/js/spanish.json"
                                },
                    'ajax'       : {
                        url: apiUrl,
                        dataType: 'json',
                        type: 'get',                    
                        contentType: 'application/json',                                                    
                    },
                    'columns'    : [
                        {data: 'product.principal_code'},
                        {data: 'quantity'},
                        {data: 'product.name'},
                        {data: 'product.generic'},
                        {data: 'product.unit_price'},
                        {
                            data: 'product.is_active',
                            render: function(status){
                                let estado = '<center><span class="label label-primary">Activo</span></center>';

                                if(status=="0"){
                                    estado = '<center><span class="label label-danger">Inactivo</span></center>';
                                }

                                return estado;
                            }    
                        },
                        // {
                        //     name:       'actions',
                        //     data:       null,
                        //     sortable:   false,
                        //     searchable: false,
                        //     render: function (data){
                        //         let actions = '';
                        //         actions += '<center>'
                        //                  + '<a href="{{url("warehouse/edit")}}/'+data.id+'" class="btn btn-xs btn-default" style="margin-right:3px"><i class="fa fa-pencil"></i></a>'
                        //                  + '<button onclick="deleteElement(' + data.id + ')" class="btn btn-xs btn-danger"> <i class="fa fa-trash"></i></button>';

                        //         return actions;
                        //     }
                        // }
                    ]
                });
            } );
        
        </script>
@endsection