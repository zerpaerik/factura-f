@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Inventario</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('home') }}">Inicio</a>
                </li>
                <li>
                    <a href="{{ url('/warehouse') }}">Almacen</a>
                </li>
                <li class="active">
                    <strong>Reportes Generales</strong>
                </li>
            </ol>
        </div>        
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">

            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Generación de Reportes</h5>                        
                    </div>                    

                    <div class="ibox-content">
                        <br>
                        <form class="form-horizontal" name="FrmGenerateReports" method="post" action="/warehouse/createPDF">   
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Sucursal</label>
                                    {{ csrf_field() }}
                                <div class="col-lg-4">
                                    <select id="branch" name="branch" chosen required>
                                          <option></option>
                                          @foreach ($branches as $branch)
                                            <option value="{{$branch->id}}">{{$branch->name}}</option>
                                          @endforeach
                                    </select>
                                </div>
                                <label class="col-lg-2 control-label">Tipo de reporte</label>
                                <div class="col-lg-3">
                                    <select id="type" name="type" chosen>
                                          <option></option>
                                           @foreach ($reports as $report)
                                            <option value="{{$report->code}}">{{$report->name}}</option>
                                          @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">       
                                <label class="col-lg-2 control-label">Fecha Inicio</label>     
                                <div class="col-lg-4">
                                    <input class="form-control centrarInput" id="startDate" name="startDate" type="date">
                                </div>
                                <label class=" col-lg-2 control-label">Fecha Fin</label>
                                <div class="col-lg-4">
                                  <input class="form-control centrarInput" id="endDate" name="endDate" type="date">
                                </div>
                            </div>

                            <div class="form-group supplier_chosen" style="display: none">
                                <label class="col-lg-2 control-label">Proveedor</label>
                                <div class="col-lg-5">
                                    <select chosen id="supplier" name="supplier">
                                          <option></option>
                                           @foreach ($suppliers as $supplier)
                                            <option value="{{$supplier->id}}">{{$supplier->social_reason}}</option>
                                          @endforeach
                                    </select>
                                </div>
                            </div>                           

                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-9">
                                    <button class="btn btn-md btn-primary" type="submit">Generar Reporte</button>
                                    <a class="btn btn-md btn-warning" href="{{url('/branch')}}">Volver</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function(){
        

        $('#type').change(function(){
            switch ($(this).val()) {
                case '1':
                    $('#startDate, #endDate').attr('disabled', false);
                break;
                case '2':
                    $('#startDate, #endDate').attr('disabled', true);
                break;
                case '6':
                    $('.supplier_chosen').show('fast');
                break;
            }
            // if ($(this).val() == '6') {
            //     $('.supplier_chosen').show('fast');
            // } else {
            //     $('.supplier_chosen').hide('fast');
            // }
        });
    });
  </script>
@endsection