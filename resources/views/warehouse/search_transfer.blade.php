@extends('layouts.app')

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Inventario</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('home') }}">Inicio</a>
                </li>
                <li>
                    <a href="{{ url('/warehouse') }}">Inventario</a>
                </li>
                <li class="active">
                    <strong>Busqueda de Transferencia</strong>
                </li>
            </ol>
        </div>        
    </div>
	<div class="wrapper wrapper-content animated fadeInRight" ng-controller="WarehouseController as vm">
        <div class="row">

            <div class="col-lg-12">
                <div class="ibox float-e-margins">    
                    <div class="ibox-title">
                        <h5>Busqueda de Transferencia</h5>                        
                    </div>       
                    <div class="ibox-content">
                        <form class="form-horizontal" name="FrmSearchTransfer" novalidate="novalidate" ng-submit="vm.acceptTransfer()">
                            
                            <div class="form-group">
                           		<label class="col-lg-2 col-lg-offset-1 control-label">N° de Documento</label>
                                <div class="col-lg-4">                                
                                    <input class="form-control centrarInput" type="text" ng-model="vm.Product.document_number" ng-change="vm.invoiceUpperCase(vm.Product, 'document_number')" value="" placeholder="Ingrese N° de Documento">
                                </div>
                                <div class="col-lg-2">                                
                                    <button type="button" class="btn btn-md btn-success" ng-click="vm.getTransfer()"><i class="fa fa-search"></i></button>
                                </div>
                            </div>  
    
                            <!-- <div class="form-group">
                                <label class="col-lg-2 col-lg-offset-1 control-label">Almacen Origen</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control centrarInput" readonly="" ng-model="vm.dataTransfer.warehouse_origin">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 col-lg-offset-1 control-label">Producto</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control centrarInput" readonly="" ng-model="vm.dataTransfer.product">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 col-lg-offset-1 control-label">Cantidad</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control centrarInput" readonly="" ng-model="vm.dataTransfer.amount_send">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 col-lg-offset-1 control-label">Fecha</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control centrarInput" readonly="" ng-model="vm.dataTransfer.created_at">
                                </div>
                            </div> -->
                            
                            <div class="row">
                                <div class="form-group">
                                <div class="col-xs-12">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Items</h5>              
                                        </div>    
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <table class="table table-responsive">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 10%">
                                                                    <center>Código</center>
                                                                </th>
                                                                <th style="width: 25%">
                                                                    <center>Nombre Producto</center>
                                                                </th>
                                                                <th style="width: 25%">
                                                                    <center>Nombre Generico</center>
                                                                </th>
                                                                <th style="width: 8%">
                                                                    <center>Cantidad</center>
                                                                </th>
                                                                <th style="width: 15%">
                                                                    <center>Origen</center>
                                                                </th>
                                                                <th style="width: 30%">
                                                                    <center>Fecha Envio</center>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr ng-repeat="itemLine in vm.Items" style="text-align: center;">
                                                                <td>
                                                                   @{{itemLine.product.principal_code}}
                                                                </td>
                                                                <td>
                                                                    @{{itemLine.product.name}}
                                                                </td> 
                                                                <td>
                                                                    @{{itemLine.product.generic}}
                                                                </td>
                                                                <td>
                                                                    @{{itemLine.amount_send}}
                                                                </td>
                                                                <td>
                                                                    @{{itemLine.warehouse_origin.name}}
                                                                </td>
                                                                <td>
                                                                    @{{itemLine.created_at}}
                                                                </td>
                                                            </tr>
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>                                   
                                            </div>                                  
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <button class="btn btn-md btn-primary" type="submit" ng-disabled="!vm.btn_accept">Transferir</button>
                                    <a class="btn btn-md btn-warning" href="{{url('/warehouse/transfers')}}">Volver</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>  
@endsection