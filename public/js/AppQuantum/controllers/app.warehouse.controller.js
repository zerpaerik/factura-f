(function () {
    "use strict";

    angular
        .module('QuantumApp')
        .controller('WarehouseController', WarehouseController);

    WarehouseController.$inject = ['$scope', '$http', 'WarehouseService','ProductService','CompanyService','SupplierService','BranchService','EntityMasterDataService','ENTITY', '$rootScope', 'toastr', 'HOST_ROUTE', 'SweetAlert'];

    function WarehouseController($scope, $http,WarehouseService,ProductService,CompanyService,SupplierService,BranchService,EntityMasterDataService,ENTITY, $rootScope, toastr, HOST_ROUTE, SweetAlert) {
        
        var vm = this;

        vm.WarehousesList = [];
        vm.WarehousesDestinationList = [];
        vm.CompanyList = [];
        vm.BranchOfficesList = [];
        vm.BranchOfficesListDes = [];
        vm.company = {};
        vm.branchOffice = {};
        vm.branchOfficeDes = {};
        vm.warehouse = [];
        vm.checked = false;
        vm.warehouseOrigin = 0;
        vm.destinationStore = 0;
        vm.Items = [];
        vm.Product = [];
        vm.document_number = '';
        vm.auditSelected = false;
        vm.invoiceNumber = "";
        vm.invoiceObservation = "";
        vm.ivaTotal = '0.00';
        vm.invoiceSubTotal = '0.00';
        vm.invoiceTotal = '0.00';
        vm.sppay = '0.00';
        vm.sppayt = '0.00';
        vm.supplierSelected = [];
        vm.dataTransfer = [{
            "warehouse_origin": "",
            "product": "",
            "amount_send": "",
            "created_at": "",
        }];

        vm.report = [{
            "branch": "",
            "type": "",
            "expiration_date": "",
            "start_date": "",
            "end_date": "",
            "supplier": "",
            "product": "",
            "vcm": false,
            "dareRange": false,
            "enableProduct": false
        }];

        vm.transfer = [{
            "company" : "",
            "warehouse_origin" : "",
            "destination_store" : "",
            "branch_origin" : "",
            "destination_branch" : "",
            "items[]" : []
        }];

        vm.btn_accept = false;

        vm.ProductsList = [];
        vm.SuppliersList = [];

        vm.typeOutputs = [
                {name:'Vencimiento de Producto', type:4},
                {name:'Donación de Producto', type:5},
                {name:'Devolución a Proveedor', type:6},
                {name:'Auditoria', type:9}
        ];

        vm.typeReports = [
            {name: 'Producto - Fecha de Vencimiento', type: 1},
            {name: 'Productos - Sucursal', type: 2},
            {name: 'Productos - Entrada - Rango Fecha', type: 3},
            {name: 'Productos - Entrada - Transferencias', type: 4},
            {name: 'Productos - Salida - Transferencias', type: 5},
            {name: 'Productos - Proveedor', type: 6},
        ];

        vm.aliquotList = [
            {aliquot: 0.10, value:'10%'},
            {aliquot: 0.20, value:'20%'},
            {aliquot: 0.25, value:'25%'}
        ];

        vm.withIva = [
            {name:'SI', value:1},
            {name:'NO', value:2}
        ];
    
        vm.create = function(redir = true){
            if(vm.toggleSelected == true)
                vm.warehouse.is_active = 1;
            else 
                vm.warehouse.is_active = 0;
            
            vm.warehouse.company_id = vm.company.id; 
            vm.warehouse.branch_id = vm.branchOffice.id; 

            var promise = WarehouseService.create(vm.warehouse);
            promise.then(function(pl){
                var retorno = pl.data;
                toastr.success("El almacen fue creada satisfactoriamente");
                if(redir)
                    window.location = HOST_ROUTE.WAREHOUSE;
                else{
                    //$rootScope.$emit("refreshClientList");
                    angular.element('#myModal').modal('hide');
                }
            }),
            function (errorPl) {
                toastr.error("Ocurrió un error al crear el proveedor" + errorPl);
            };
        }

        vm.update = function(){
            if(vm.toggleSelected==true)
                vm.warehouse.is_active=1;
            else 
                vm.warehouse.is_active=0;
            var promise = WarehouseService.update(vm.warehouse, vm.warehouse.id);
            promise.then(function(pl){
                var retorno = pl.data;
                toastr.success("El almacen fué actualizado satisfactoriamente");
                window.location = HOST_ROUTE.WAREHOUSE;
            }),
            function (errorPl) {
                toastr.error("Ocurrió un error al actualizar familia" + errorPl);
            };
        }  

        vm.delete = function(id){
            SweetAlert.swal({
                  title: 'Eliminar registro',
                  text: "¿Está seguro que desea eliminar el registro?",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si',
                  cancelButtonText: 'No',
                  confirmButtonClass: 'btn btn-success',
                  cancelButtonClass: 'btn btn-danger',
                  buttonsStyling: false
            }, function (dismiss) {
              if (dismiss === true) {
                var promise = WarehouseService.delete(id);
                promise.then(function(pl){
                    toastr.success("El registro fue eliminado satisfactoriamente");
                    window.location = HOST_ROUTE.WAREHOUSE;    
                });
              }
            }); 
        }   

        vm.edit = function(id){
            var promise = WarehouseService.read(id);
            promise.then(function(pl){
                vm.warehouse = pl.data;
                if(vm.warehouse.is_active===1) 
                    vm.toggleSelected = true;
                else
                    vm.toggleSelected = false;
            });
        }

        vm.readAll = function(){
            var promise = WarehouseServices.readAll();
            promise.then(function(pl){
                vm.WarehousesList = pl.data;
            }); 
        }

        vm.readCompanyList = function(UserRoleId, CompanyId, BranchId = null){
            var promise = CompanyService.readAll();
            promise.then(function(pl){
                if(UserRoleId===1)// SuperAdministrador
                   vm.CompanyList = pl.data;
                else
                {
                   var datos = pl.data;
                   angular.forEach(datos, function(company, key){
                        if(company.id===CompanyId){
                            vm.CompanyList.push(company);
                            vm.company = company;
                            vm.getAllBranchOffice(UserRoleId,CompanyId,BranchId);
                        }
                   });
                }
            });    
        }

        vm.readWarehouseList = function(UserRoleId, BranchId){
            var promise = WarehouseService.readAll();
            vm.WarehousesList = [];
            promise.then(function(pl){
                // if(UserRoleId===1 || UserRoleId ===2 || UserRoleId ===3)// SuperAdministrador || Administrador Sucursal
                //    vm.WarehousesList = pl.data;
                // else
                // {
                   var datos = pl.data;
                   angular.forEach(datos, function(warehouse, key){
                        if(warehouse.branch_id===BranchId){
                            vm.WarehousesList.push(warehouse);
                        }
                   });
                // }
            });    
        }

        vm.readWarehouseDestinationList = function(UserRoleId, BranchId){
            if (vm.branchOffice.id == vm.branchOfficeDes.id) {
                SweetAlert.swal("Linea de Transferencia!", "La sucursal destino no pude ser igual al origen!", "warning"); 
                vm.branchOfficeDes = {};
            } else {
                var promise = WarehouseService.readAll();
                promise.then(function(pl){
                    // if(UserRoleId===1 || UserRoleId ===2 || UserRoleId ===3)// SuperAdministrador || Administrador Sucursal
                    //    vm.WarehousesDestinationList = pl.data;
                    // else
                    // {
                       var datos = pl.data;
                       angular.forEach(datos, function(warehouse, key){
                            if(warehouse.branch_id === BranchId){
                                vm.WarehousesDestinationList.push(warehouse);
                            }
                       });
                    // }
                });    
            }
        }

        vm.selectOrigin = function(CompanyId, BranchId){
            var promise = WarehouseService.getProducts(vm.warehouseSelected.id, CompanyId, BranchId);
            promise.then(function(pl){
                vm.ProductsList = pl.data;
            });
        }

        vm.deleteItemLine = function(itemLine){
            var index = vm.Items.indexOf(itemLine);
            vm.Items.splice(index, 1);

            let subTotal = parseFloat(vm.invoiceSubTotal) - parseFloat(itemLine.product.invoiceAmount);
            vm.invoiceSubTotal = subTotal.toFixed(2);

            let sppay = parseFloat(vm.sppay) - parseFloat(itemLine.sppay);
            vm.sppay = sppay.toFixed(2);

            if (vm.auditSelected) {
                vm.invoiceTotal = vm.invoiceSubTotal;
            } else {
                let total = parseFloat(vm.ivaTotal) - parseFloat(itemLine.iva);
                vm.ivaTotal = total.toFixed(2);

                let totalG = parseFloat(vm.invoiceSubTotal) + parseFloat(vm.ivaTotal);
                vm.invoiceTotal = totalG.toFixed(2);

                let pt = parseFloat(vm.invoiceSubTotal) - parseFloat(vm.sppay);
                vm.sppayt = pt.toFixed(2);
            }
        }

        vm.selectProduct = function(idCompany, BranchId){

            var promise = WarehouseService.readWP(idCompany, vm.productSelected.id, vm.warehouseSelected.id, BranchId);
            promise.then(function(pl){
                var n =  new Date();
                if(pl.data == 0){
                    vm.Product.product_id  = vm.productSelected.id;
                    vm.Product.name        = vm.productSelected.name;
                    vm.Product.description = vm.productSelected.description;
                    vm.Product.transferAmount = 0; 
                    vm.Product.currentAmount = 0;
                    vm.Product.invoiceAmount = 0;
                    vm.Product.expiration_date = new Date(n.getFullYear(),n.getMonth(),n.getDate()); 
                    vm.Product.unit_cost = vm.productSelected.unit_cost;
                    
                    vm.Product.laboratory   = vm.productSelected.laboratory;
                    vm.Product.location     = vm.productSelected.location;
                    vm.Product.expired_date = vm.productSelected.expired_date;
                    vm.Product.generic      = vm.productSelected.generic;
                } else {
                    vm.Product.product_id  = vm.productSelected.id;
                    vm.Product.name        = pl.data.product.name;
                    vm.Product.description = pl.data.product.description;
                    vm.Product.transferAmount = 0; 
                    vm.Product.invoiceAmount = 0;
                    vm.Product.currentAmount = pl.data.quantity;
                    vm.Product.expiration_date = new Date(n.getFullYear(),n.getMonth(),n.getDate()); 
                    vm.Product.unit_cost = pl.data.product.unit_cost;
                    
                    if(pl.data.product.laboratory   !== null) vm.Product.laboratory   = pl.data.product.laboratory;
                    if(pl.data.product.location     !== null) vm.Product.location     = pl.data.product.location;
                    if(pl.data.product.expired_date !== null) vm.Product.expired_date = pl.data.product.expired_date;
                    if(pl.data.product.generic      !== null) vm.Product.generic      = pl.data.product.generic;
                }

            });
        }

        vm.getAllProducts = function(idCompany){
            var promise = ProductService.readAll(idCompany);
            promise.then(function(pl){
                vm.ProductsList = pl.data;
            }); 
        }

        vm.getAllBranchOffice = function(UserRoleId, idCompany, branchId = null, transfer = false){
            var promise = BranchService.readByCompany(idCompany);
            promise.then(function(pl){
                if(UserRoleId===1 || UserRoleId ===2 || UserRoleId ===3)// SuperAdministrador || Administrador Sucursal
                   vm.BranchOfficesList = pl.data;
                else
                {
                   var datos = pl.data;
                   angular.forEach(datos, function(branch, key){
                        if(branch.id===branchId){
                            vm.BranchOfficesList.push(branch);
                        }
                   });
                }
            });
        
            vm.getAllProducts(); 

            if (transfer) {
                vm.getAllBranchOfficeDes(UserRoleId, idCompany, branchId);
                vm.correlativeTransfer(idCompany, branchId);
            } else {
                vm.correlativeAuditEntry(idCompany, branchId);
                vm.suppliers();
            }
        }

        vm.getAllBranchOfficeDes = function(UserRoleId, idCompany, branchId){
            var promise = BranchService.readByCompany(idCompany);
            promise.then(function(pl){
                if(UserRoleId===1 || UserRoleId ===2 || UserRoleId ===3)// SuperAdministrador || Administrador Sucursal
                   vm.BranchOfficesListDes = pl.data;
                else
                {
                   var datos = pl.data;
                   angular.forEach(datos, function(branch, key){
                        if(branch.id != branchId){
                            vm.BranchOfficesListDes.push(branch);
                        }
                   });
                }
            });
            
            vm.correlativeTransfer(idCompany, branchId);
            vm.getAllProducts(); 
        }

        vm.addItemLine = function(product, option, company = null, branch = null){
            var itemLine = {};
            var subTotal = '0.00';
            
            vm.Items = vm.Items || [];

            if(option === 1){
                if(product.transferAmount<=0){
                    SweetAlert.swal("Cantidad incorrecta!", "La Cantidad a transferir debe ser mayor a cero!", "warning"); 
                    return;
                }
                if(isNaN(parseInt(product.transferAmount)) && option === 1){
                    SweetAlert.swal("Cantidad incorrecta!", "La Cantidad a transferir acepta solo valores numericos!", "warning"); 
                    return;
                }

                itemLine.product = {
                    product_id   : product.product_id,
                    principal_code : product.product_id.principal_code,
                    name         : product.name,
                    description  : product.description,
                    document_number : product.documentNumber,
                    laboratory   : product.product_id.laboratory ? product.product_id.laboratory : "",
                    location     : product.product_id.location ? product.product_id.location : "",
                    generic      : product.product_id.generic ? product.product_id.generic : ""
                };
                itemLine.currentAmount = product.currentAmount;
                itemLine.transferAmount = product.transferAmount;
            } else {
                
                if (vm.supplierSelected.length == 0) {
                    SweetAlert.swal("Entrada de Producto!", "Debe seleccionar un proveedor!", "warning"); 
                    vm.productSelected = "";
                    return;
                }
                if(product.product_id==="" || product.product_id===undefined){
                    SweetAlert.swal("Entrada de Producto!", "Debe seleccionar un producto!", "warning"); 
                    return;
                }
                if(product.transferAmount<=0){
                    SweetAlert.swal("Cantidad incorrecta!", "La Cantidad a ingresar debe ser mayor a cero!", "warning");
                    return;
                }
                if(isNaN(parseInt(product.transferAmount))){
                    SweetAlert.swal("Cantidad incorrecta!", "La Cantidad a ingresar acepta solo valores numericos!", "warning"); 
                    return;
                }

                if(!vm.auditSelected){
                    if(product.invoiceAmount<=0){
                        SweetAlert.swal("Entrada de producto!", "El Monto de la Factura debe ser mayor a cero!", "warning");
                        return;
                    }
                    if (product.iva === undefined) {
                        SweetAlert.swal("Entrada de producto!", "Debe indicar si el producto posee iva!", "warning");
                        return;
                    }
                }

                if(!vm.auditSelected){
                    itemLine.product = {
                         product_id   : product.product_id,
                         principal_code : product.product_id.principal_code,
                         name         : product.name,
                         description  : product.description,
                         invoiceAmount : product.invoiceAmount,
                         document_number : product.documentNumber,
                         expiration_date : product.expiration_date,
                         laboratory   : product.product_id.laboratory ? product.product_id.laboratory : "",
                         location     : product.product_id.location ? product.product_id.location : "",
                         generic      : product.product_id.generic ? product.product_id.generic : "",
                         iva          : product.iva.value
                    };
                    itemLine.aliquot = '';
                    let priceCost = product.invoiceAmount / product.transferAmount;
                    itemLine.priceCost = priceCost.toFixed(2);

                    let sppay = parseFloat(product.invoiceAmount) * 0.01;
                    itemLine.sppay = sppay.toFixed(2);
                    sppay = parseFloat(vm.sppay) + parseFloat(sppay.toFixed(2));
                    vm.sppay = sppay.toFixed(2);
                    //itemLine.salePrice = 0;

                    var promise = WarehouseService.averageCostProduct(company, branch,product.product_id);
                    promise.then(function(pl){
                        let inventary_cost = parseFloat(pl.data.inventary_cost) + parseFloat(product.invoiceAmount); 
                        let quantity = (pl.data.quantity == null) ? product.transferAmount : parseInt(pl.data.quantity) + parseInt(product.transferAmount);
                        let average_cost = inventary_cost / quantity; 
                        itemLine.salePrice = itemLine.average_cost = average_cost.toFixed(2);
                    });
                    
                    if (product.iva.value == 1) {
                        let iva = parseFloat(product.invoiceAmount) * 0.12;
                        itemLine.iva = iva.toFixed(2);

                        let total = parseFloat(vm.ivaTotal) + parseFloat(iva.toFixed(2));
                        vm.ivaTotal = total.toFixed(2);
                    } else {
                        itemLine.iva = '0.00';
                    }

                    subTotal = parseFloat(vm.invoiceSubTotal) + parseFloat(product.invoiceAmount);
                        
                } else {
                    let invoiceAmount = parseFloat(product.unit_cost) * parseFloat(product.transferAmount);
                    itemLine.product = {
                         product_id   : product.product_id,
                         principal_code : product.product_id.principal_code,
                         name         : product.name,
                         description  : product.description,
                         invoiceAmount : invoiceAmount.toFixed(2),
                         document_number : "",
                         expiration_date : product.expiration_date,
                         laboratory   : product.product_id.laboratory ? product.product_id.laboratory : "",
                         location     : product.product_id.location ? product.product_id.location : "",
                         generic      : product.product_id.generic ? product.product_id.generic : ""
                    };

                    itemLine.priceCost = product.unit_cost;
                    subTotal = parseFloat(vm.invoiceSubTotal) + parseFloat(invoiceAmount);
                }
                
                vm.invoiceSubTotal = subTotal.toFixed(2);
                let totalG = parseFloat(vm.invoiceSubTotal) + parseFloat(vm.ivaTotal);
                vm.invoiceTotal = totalG.toFixed(2);

                let pt = parseFloat(vm.invoiceSubTotal) - parseFloat(vm.sppay);
                vm.sppayt = pt.toFixed(2);

                itemLine.currentAmount = product.currentAmount;
                itemLine.transferAmount = product.transferAmount;

                itemLine.modification = false;
            }

            vm.Items.push(itemLine);
            vm.resetItemLine();
        }

        vm.editInvoice = function (invoice, userRole, company, branch) {
            let itemLine = {};
            var subTotal = '0.00';
            vm.Items = vm.Items || [];
            vm.invoiceNumber = invoice;
            var promise = WarehouseService.editInvoice(invoice);
            promise.then(function(pl){
                let datos = pl.data;
                angular.forEach(datos, function(product, key){

                    if (key == 0) {
                        vm.supplierSelected = product.supplier;
                        vm.warehouseSelected = product.warehouse_origin;
                        vm.branchOffice = product.branch_origin;
                    }
                    
                    itemLine.product = {
                         record_id    : product.id,
                         product_id   : product.product.id,
                         principal_code : product.product.principal_code,
                         name         : product.product.name,
                         description  : product.product.description,
                         invoiceAmount : product.invoiceAmount,
                         lastInvoiceAmount : product.invoiceAmount,
                         document_number : "",
                         expiration_date : product.expiration_date,
                         laboratory   : product.product.laboratory ? product.product.laboratory : "",
                         location     : product.product.location ? product.product.location : "",
                         generic      : product.product.generic ? product.product.generic : "",
                         iva          : product.iva
                    };

                    if (product.aliquot == '0.1') {
                        itemLine.aliquot = {aliquot: 0.10, value:'10%'};
                    } else if (product.aliquot == '0.2') {
                        itemLine.aliquot = {aliquot: 0.20, value:'20%'};
                    } else {
                        itemLine.aliquot = {aliquot: 0.25, value:'25%'};
                    }

                    itemLine.priceCost = product.unit_cost;
                    itemLine.sppay = product.sppay;
                    itemLine.average_cost = product.average_cost;
                    itemLine.salePrice = product.product.unit_price;
                    itemLine.iva = product.product.iva;

                    itemLine.currentAmount = product.quantity_origin;
                    itemLine.transferAmount = product.amount_send;
                    itemLine.lastTransfer = product.amount_send;

                    itemLine.modification = true;

                    vm.Items.push(itemLine);
                    itemLine = {};

                    subTotal = parseFloat(subTotal) + parseFloat(product.invoiceAmount);
                    vm.invoiceSubTotal = subTotal.toFixed(2);

                    vm.ivaTotal = parseFloat(vm.ivaTotal) + parseFloat(product.iva);

                    vm.invoiceTotal = parseFloat(vm.invoiceSubTotal) + parseFloat(vm.ivaTotal);
                    
                    vm.sppay = parseFloat(vm.sppay) + parseFloat(product.sppay);

                    let pt = parseFloat(vm.invoiceSubTotal) - parseFloat(vm.sppay);
                    vm.sppayt = pt.toFixed(2);
                });
            });

            vm.getAllBranchOffice(userRole, company, branch);
            vm.readWarehouseList(userRole, branch);
            vm.suppliers();
            vm.getAllProducts(company);
        }

        vm.updatePrice = function(itemLine, company, branch) {
            let subTotal = 0;
            let cost = itemLine.product.invoiceAmount/itemLine.transferAmount;
            itemLine.priceCost = cost.toFixed(2);

            var promise = WarehouseService.averageCostProduct(company, branch,itemLine.product.product_id);
            promise.then(function(pl){
                let inventary_cost = parseFloat(pl.data.inventary_cost) + parseFloat(itemLine.product.invoiceAmount); 
                let quantity = (pl.data.quantity == null) ? itemLine.transferAmount : parseInt(pl.data.quantity) + parseInt(itemLine.transferAmount);
                let average_cost = inventary_cost / quantity; 
                itemLine.salePrice = itemLine.average_cost = average_cost.toFixed(2);
            });
            
            itemLine.aliquot = {};

            subTotal = parseFloat(vm.invoiceSubTotal) - parseFloat(itemLine.product.lastInvoiceAmount);
            subTotal = parseFloat(subTotal) + parseFloat(itemLine.product.invoiceAmount);
            vm.invoiceSubTotal = subTotal.toFixed(2);

            //Pendiente para resolver
            let iva1 = parseFloat(vm.ivaTotal) - parseFloat(itemLine.iva);
            console.log('iva 1 '+iva1);
            let iva2 = parseFloat(iva1) + (parseFloat(itemLine.product.invoiceAmount) * 0.12);
            console.log('iva 2 '+iva2);
            vm.ivaTotal = iva2.toFixed(2);
            

            vm.invoiceTotal = parseFloat(vm.invoiceSubTotal) + parseFloat(vm.ivaTotal);
            
            let sppay = parseFloat(itemLine.product.invoiceAmount) * 0.01;
            let sppay1 =  itemLine.sppay;

            itemLine.sppay = sppay.toFixed(2);
            let r = parseFloat(vm.sppay) - parseFloat(sppay1);
            r = parseFloat(r) + parseFloat(sppay);
            vm.sppay = r.toFixed(2);

            let pt = parseFloat(vm.invoiceSubTotal) - parseFloat(vm.sppay);
            vm.sppayt = pt.toFixed(2);
        }

        vm.resetItemLine = function(){
            vm.Product = [];
            vm.productSelected = "";
        }

        vm.addEntryProduct = function (idCompany) {

            if(!vm.auditSelected){
                if(vm.supplierSelected === undefined){
                        SweetAlert.swal("Entrada de producto!", "Debe de seleccionar un Proveedor!", "warning"); 
                        return;
                    }
                if(vm.invoiceNumber === '' || vm.invoiceNumber === undefined){
                    SweetAlert.swal("Enrada de producto!", "El número de factura debe ser ingresado!", "warning"); 
                    return;
                }
            } else {
                vm.invoiceNumber = vm.auditNumber;
            }
            
            let supplier = (vm.auditSelected) ? 0 : vm.supplierSelected.id;
            var promise = WarehouseService.createEntryProduct(vm.branchOffice.id,vm.invoiceNumber, vm.invoiceObservation, vm.Items, idCompany, vm.warehouseSelected.id, vm.auditSelected, supplier);
            promise.then(function(pl){
                var retorno = pl.data;
                if(retorno == 1){
                    toastr.success("Los productos han sido registrado satisfactoriamente");
                    window.location = HOST_ROUTE.WAREHOUSE+'/product_entry';
                } else {
                    toastr.error("Los productos no han sido registrado satisfactoriamente");
                }
            });
        }

        vm.updateEntryProduct = function (idCompany) {
            if(vm.invoiceNumber === '' || vm.invoiceNumber === undefined){
                SweetAlert.swal("Enrada de producto!", "El número de factura debe ser ingresado!", "warning"); 
                return;
            }

            var promise = WarehouseService.updateEntryProduct(vm.branchOffice.id,vm.invoiceNumber, vm.invoiceObservation, vm.Items, idCompany, vm.warehouseSelected.id, vm.supplierSelected.id);
            promise.then(function(pl){
                var retorno = pl.data;
                if(retorno == 1){
                    toastr.success("El documento han sido actualizado satisfactoriamente");
                    window.location = HOST_ROUTE.WAREHOUSE+'/documents';
                } else {
                    toastr.error("El documentos no han sido actualizado satisfactoriamente");
                }
            });
        }

        vm.addOutputProduct = function (idCompany) {
            if(vm.Product.transferAmount === 0){
                SweetAlert.swal("Linea de Salida!", "La cantidad de salida debe ser mayor a 0!", "warning"); 
                return;
            }

            var promise = WarehouseService.createOutputProduct(idCompany,vm.branchOffice.id,vm.productSelected.id,vm.warehouseSelected.id,vm.Product.transferAmount,vm.Product.observation, vm.typeOutput.type, vm.documentNumber);
            promise.then(function(pl){
                var retorno = pl.data;
                if(retorno == 1){
                    toastr.success("La salida del producto ha sido registrada satisfactoriamente");
                    window.location = HOST_ROUTE.WAREHOUSE+'/createOutput';
                } else {
                    toastr.error("La salida del producto no ha sido registrada satisfactoriamente");
                }
            });
        }

        vm.validateAmountTransfer = function (product) {
            if(product.transferAmount > product.currentAmount){
                SweetAlert.swal("Linea de Transferencia!", "La cantidad a transferir es mayor que la cantidad actual del producto!", "warning"); 
                product.transferAmount = 0;
            }
        }

        vm.validateWarehouse = function () {
            if(vm.destinationStore.id == vm.warehouseSelected.id){
                SweetAlert.swal("Linea de Transferencia!", "El almacen destino no pude ser igual al origen!", "warning"); 
                vm.destinationStore = '';
            }
        }

        vm.branchOfficeValidate = function (UserRoleId) {
            if (vm.branchOffice.id == vm.branchOfficeDes.id) {
                SweetAlert.swal("Linea de Transferencia!", "La sucursal destino no pude ser igual al origen!", "warning"); 
                vm.branchOfficeDes = {};
            } else {
                vm.readWarehouseDestinationList(UserRoleId,vm.branchOfficeDes.id);
            }
        }

        vm.createTransfer = function (idCompany) {
            vm.transfer.company = idCompany;
            vm.transfer.warehouse_origin = vm.warehouseSelected.id;
            vm.transfer.destination_store = vm.destinationStore.id;
            vm.transfer.branch_origin = vm.branchOffice.id;
            vm.transfer.destination_branch = vm.branchOfficeDes.id;
            vm.transfer.items = vm.Items;
            vm.transfer.document_num = vm.document_number;

            // var promise = WarehouseService.createTransfer(idCompany, vm.warehouseSelected.id, vm.destinationStore.id, vm.Items);
            var promise = WarehouseService.createTransfer(vm.transfer);
            promise.then(function(pl){
                var retorno = pl.data;
                if(retorno == 1){
                    toastr.success("Los productos han sido transferidos satisfactoriamente");
                    window.location = HOST_ROUTE.WAREHOUSE+'/createTransfer';
                } else {
                    toastr.error("Los productos no han sido transferidos satisfactoriamente");
                }
            })
        }

        vm.suppliers = function () {
            var promise = SupplierService.readAll();
            promise.then(function(pl){
                vm.SuppliersList = pl.data;
            })
        }

        vm.invoiceUpperCase = function (property){  
            if(property == 'invoiceNumber'){
                vm.invoiceNumber = vm.invoiceNumber.toUpperCase();
            } else {
                vm.document_number = vm.document_number.toUpperCase();
            }
        }

        vm.getTransfer = function () {
            var promise = WarehouseService.getTransfer(vm.Product.document_number);
            promise.then(function(pl){
                if(pl.data.length == 0){
                    toastr.error("El documneto de transferencia no existe.");
                    vm.Product.document_number = '';
                    // vm.dataTransfer.warehouse_origin = "";
                    // vm.dataTransfer.product = "";
                    // vm.dataTransfer.amount_send = "";
                    // vm.dataTransfer.created_at = "";
                    vm.btn_accept = !vm.btn_accept;
                } else {
                    vm.Items = pl.data;

                    // vm.dataTransfer.warehouse_origin = pl.data.warehouse_origin.name;
                    // vm.dataTransfer.product = pl.data.product.name;
                    // vm.dataTransfer.amount_send = pl.data.amount_send;
                    // vm.dataTransfer.created_at = pl.data.created_at;
                    vm.btn_accept = !vm.btn_accept;
                }
            })
        }
        
        vm.acceptTransfer = function () {
            if (vm.Product.document_number === '' || vm.Product.document_number === undefined) {
                toastr.error("Debe de ingresar un Número de Documento.");
                vm.Items = [];
                return false;
            }

            var promise = WarehouseService.acceptTransfer(vm.Product.document_number);
            promise.then(function(pl){
                if(pl.data == 1){
                    vm.dataTransfer.warehouse_origin = "";
                    vm.dataTransfer.product = "";
                    vm.dataTransfer.amount_send = "";
                    vm.dataTransfer.created_at = "";
                    vm.btn_accept = !vm.btn_accept;
                    toastr.success("El inventario ha sido actualizado satisfactoriamente");
                    window.location = HOST_ROUTE.WAREHOUSE+'/searchTransfer';
                } else {
                    toastr.error("Error en el sistema al momento de actualizar el inventario.");
                }
            })
        }

        vm.audit = function () {
            if (vm.auditSelected) {
                vm.SuppliersList = [];
                vm.Items = [];
                vm.productSelected = "";
                vm.ivaTotal = vm.invoiceSubTotal = vm.invoiceTotal = '0.00';
            } else {
                vm.suppliers();
                vm.Items = [];
                vm.invoiceNumber = '';
                vm.productSelected = "";
                vm.ivaTotal = vm.invoiceSubTotal = vm.invoiceTotal = '0.00';
            }
        }

        vm.correlativeTransfer = function (idCompany, idBranch) {
            var promise = WarehouseService.correlativeTransfer(idCompany, idBranch);
            promise.then(function(pl){
                vm.document_number = pl.data; 
            });
        }

        vm.correlativeAuditEntry = function (idCompany, idBranch) {
            var promise = WarehouseService.correlativeAuditEntry(idCompany, idBranch);
            promise.then(function(pl){
                vm.auditNumber = pl.data; 
            });
        }

        vm.correlativeTypeOutput = function (idCompany, idBranch, typeOutput){
            var promise = WarehouseService.correlativeExitProduct(idCompany, idBranch, typeOutput);
            promise.then(function(pl){
                vm.documentNumber = pl.data; 
            });
        }
        
        vm.generateReport = function () {
            if (vm.report.branch == undefined) {
                SweetAlert.swal("Generación de reportes!", "Debe seleccionar una sucursal!", "warning"); 
                return false;
            }

            if (vm.report.type ==  undefined) {
                SweetAlert.swal("Generación de reportes!", "Debe seleccionar un tipo de reporte!", "warning"); 
                return false;
            }
        }

        vm.calculatePrice = function (itemLine) {
            let salePrice = parseFloat((parseFloat(itemLine.average_cost) * parseFloat(itemLine.aliquot.aliquot)) + parseFloat(itemLine.average_cost));
            itemLine.salePrice = salePrice.toFixed(2);
        }
    }
})();