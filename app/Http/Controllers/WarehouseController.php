<?php

namespace App\Http\Controllers;
use App\Helpers\LoginChecker;
use App\Helpers\ValidateAccess;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Cookie;
use PDF;

class WarehouseController extends Controller
{
    public function __construct(Redirector $redirect){
        if(LoginChecker::check() !== true){                        
           $redirect->to('login')->send();                     
        }
    }
    
	public function index()
    {
        if(ValidateAccess::checkAccess(env('MODULE_WAREHOUSE', NULL), env('PERMISSION_READ', NULL)))
            return view('warehouse.index');
        else
            return redirect('error');
    }

    public function create()
    {
        if(ValidateAccess::checkAccess(env('MODULE_WAREHOUSE', NULL), env('PERMISSION_CREATE', NULL)))
    	   return view('warehouse.create');
        else
            return redirect('error');
    }
    
    public function edit($id)
    {
        if(ValidateAccess::checkAccess(env('MODULE_WAREHOUSE', NULL), env('PERMISSION_UPDATE', NULL)))
    	   return view('warehouse.edit', ['id'=>$id]);
        else
            return redirect('error');
    }

    public function product_entry()
    {
        return view('warehouse.product_entry');
    }

    public function movements()
    {
        return view('warehouse.list_movements'); 
    }

    public function createTransfer()
    {
        // if(ValidateAccess::checkAccess(env('MODULE_WAREHOUSE', NULL), env('PERMISSION_TRANSFER_PRODUCT', NULL)))
           return view('warehouse.createTransfer');
        // else
            // return redirect('error');
    }

    public function createOutput()
    {
        // if(ValidateAccess::checkAccess(env('MODULE_WAREHOUSE', NULL), env('PERMISSION_EXIT_PRODUCT', NULL)))
           return view('warehouse.createOutput');
        // else
            // return redirect('error');
    }

    public function getTransfer()
    {
        // if(ValidateAccess::checkAccess(env('MODULE_WAREHOUSE', NULL), env('PERMISSION_PRODUCT_RECEIPT', NULL)))
           return view('warehouse.search_transfer');
        // else
            // return redirect('error');
    }

    public function reports()
    {
        // if(ValidateAccess::checkAccess(env('MODULE_WAREHOUSE', NULL), env('PERMISSION_GENERATE_REPORT', NULL))){
            $client = new Client();

            if (Cookie::get('userRole') == 1) {
                $res = $client->request('GET', '' . env('API_HOST', NULL) . '/' . 'branch/all/0');
            } else {
                $res = $client->request('GET', '' . env('API_HOST', NULL) . '/' . 'branch/all/' . Cookie::get('company_id'));
            }

            $typeReports = $client->request('GET', '' . env('API_HOST', NULL) . '/' . 'reportsWP');

            $suppliers = $client->request('GET', '' . env('API_HOST', NULL) . '/' . 'supplier');

            if($res->getStatusCode() == 200 && $typeReports->getStatusCode() == 200 && $suppliers->getStatusCode() == 200){                        
                $res->getBody()->rewind();
                $typeReports->getBody()->rewind();
                $suppliers->getBody()->rewind();           
                $contentBranches        = $res->getBody()->getContents();
                $contentReports         = $typeReports->getBody()->getContents();
                $contentSuppliers         = $suppliers->getBody()->getContents();
                $branches  = json_decode($contentBranches);
                $reports  = json_decode($contentReports);   
                $suppliers  = json_decode($contentSuppliers);  

                $newBO = array();
                
                if (Cookie::get('userRole') == 7) {
                    foreach ($branches as $branch) {
                        if (Cookie::get('branch_office_id') == $branch->id) {
                            $newBO[] =  $branch;
                        };
                    }
                    $branches = $newBO;
                }
                       

                return view('warehouse.reports', compact('branches', 'reports', 'suppliers'));
            }
            else{
                return response('', 404);
            }
        // } else {
        //     return redirect('error');
        // }
    }

    public function createPDF(Request $request)
    {
        switch ($request->get('type')) {
            case '1':
                $this->getProductExpirationDate(Cookie::get('company_id'), $request->get('branch'), $request->get('startDate'), $request->get('endDate'));
                break;
            case '2':
                $this->getProductBranch(Cookie::get('company_id'), $request->get('branch'));
                break;
        }
    }

    public function getProductExpirationDate($company, $branch, $startDate, $endDate)
    {
        $client = new Client();
        $res = $client->request('GET', '' . env('API_HOST', NULL) . '/' . 'warehouse/productByExpirationDate/' . $company . '/' . $branch . '/' . $startDate . '/' .$endDate);
        if($res->getStatusCode() == 200){                        
            $res->getBody()->rewind();            
            $result         = $res->getBody()->getContents();
            $resultObject  = json_decode($result);            
            
            $pdf = PDF::loadView('pdf.productByExpirationDate', compact('resultObject'), [], ['format' => 'A4']);
            return $pdf->download('productByExpirationDate_'.date('d-m-Y', strtotime($startDate)).'_'.date('d-m-Y', strtotime($endDate)).'.pdf');
        }
        else{
            return response('', 404);
        }
    }

    public function getProductBranch($company, $branch)
    {
        $client = new Client();
        $res = $client->request('GET', '' . env('API_HOST', NULL) . '/' . 'warehouse/productBranch/' . $company . '/' . $branch);
        if($res->getStatusCode() == 200){                        
            $res->getBody()->rewind();            
            $result         = $res->getBody()->getContents();
            $resultObject  = json_decode($result);             
            
            $pdf = PDF::loadView('pdf.productBranch', compact('resultObject'), [], ['format' => 'A4']);
            return $pdf->download('productBranch_'.date('d-m-Y').'.pdf');
        }
        else{
            return response('', 404);
        }
    }

    public function stock()
    {
        return view('warehouse.stock');
    }

    public function stockPDF()
    {
        set_time_limit(0);
        $client = new Client();
        $res = $client->request('GET', '' . env('API_HOST', NULL) . '/' . 'warehouseStock/' . Cookie::get('company_id') . '/' .Cookie::get('branch_office_id'));
        if($res->getStatusCode() == 200){                        
            $res->getBody()->rewind();            
            $result         = $res->getBody()->getContents();
            $resultObject  = json_decode($result);           
           
            $pdf = PDF::loadView('pdf.stock', compact('resultObject'), [], ['format' => 'A4']);
            return $pdf->download('stock_'.date('d-m-Y').'.pdf');
        }
        else{
            return response('', 404);
        }
    }

    public function documents()
    {
        return view('warehouse.list_documents');
    }

    public function getProducts($invoice)
    {
        return view('warehouse.edit_invoice_product', compact('invoice'));
    }
}